import numpy


class Instrument:
    def __init__(self, instrument_id, instrument_name, base_price, drift, variance):
        self.id = instrument_id
        self.name = instrument_name
        self.base_price = base_price
        self.price = base_price
        self.drift = drift
        self.variance = variance

    def calculateNextPrice(self, direction):
        newPriceStarter = self.price + numpy.random.normal(0, 1) * self.variance + self.drift
        newPrice = newPriceStarter if (newPriceStarter > 0) else 0.0
        if self.price < self.base_price * 0.4:
            self.drift = (-0.7 * self.drift)
        self.price = newPrice * 1.01 if direction == 'B' else newPrice * 0.99
        return self.price
