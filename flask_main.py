from flask import Flask, Response
from configparser import ConfigParser
from random_deal_data import RandomDealData
import requests

config = ConfigParser()
config.read("./config.ini")

server_config = config["server"]
dao_server_config = config["dao-server"]

random_deal_data = RandomDealData()
instrument_list = random_deal_data.createInstrumentList()

app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/data_stream")
def stream():
    def event_stream():
        while True:
            json_str = random_deal_data.createRandomData(instrument_list)
            try:
                requests.post("{}://{}:{}/data_stream".format(dao_server_config["protocol"],
                                                              dao_server_config["host"],
                                                              dao_server_config["port"]), json=json_str)
            except:
                print("Error posting data to DAO layer. Probably the DAO server is down.")
            yield json_str + "\n"

    return Response(event_stream(), mimetype="text/event-stream")


def boot_app():
    app.run(port=server_config["port"], threaded=True, host=server_config["host"])
